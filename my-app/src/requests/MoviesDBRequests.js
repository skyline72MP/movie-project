const MAIN_URL = 'http://localhost:3001/';

export const getMovie = (id) => {

    return fetch(MAIN_URL + `movies/${id}`, {
        headers: {
          'Content-Type': 'application/json',
        }
    })

        .then(res => res.json())

};
export const getMovieFavorites = () => {

  return fetch(MAIN_URL + `movies/`, {
      headers: {
        'Content-Type': 'application/json',
      }
  })

      .then(res => res.json())

};
export const uploadMoviesId = (id) => {

    const request = {
              method: 'POST',
              headers: {
                'Content-Type': 'application/json',
              },
            };
    return fetch(MAIN_URL + `movies/${id}`, request)

        .then(res => res.json())

};
export const setFavoriteMovie = (id) => {

    const request = {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
            };
    return fetch(MAIN_URL + `movies/${id}`, request)

        .then(res => res.json())

};
export const unSetFavoriteMovie = (id) => {

    const request = {
              method: 'DELETE',
              headers: {
                'Content-Type': 'application/json',
              },
            };
    return fetch(MAIN_URL + `movies/${id}`, request)

        .then(res => res.json())

};

// export const createPost = (post) => {
//     const request = {
//       method: 'POST',
//       headers: {
//         'Content-Type': 'application/json',
//       },
//       body: JSON.stringify(post)
//     };
  
//     return fetch(MAIN_URL + 'posts', request)
//       .then(res => res.json())
//   };