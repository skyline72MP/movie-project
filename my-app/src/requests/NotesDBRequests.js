const MAIN_URL = 'http://localhost:3001/';

export const setNotesDB = (id, note) => {

    const noteData = {
        note: note
    };

    const request = {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(noteData)
            };
    return fetch(MAIN_URL + `notes/${id}`, request)

        .then(res => res.json())

};