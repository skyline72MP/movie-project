const MAIN_URL = 'http://localhost:3001/';

export const setRatingDB = (id, number) => {

    const vote = {
        num: number
    };

    const request = {
              method: 'PUT',
              headers: {
                'Content-Type': 'application/json',
              },
              body: JSON.stringify(vote)
            };
    return fetch(MAIN_URL + `ratings/${id}`, request)

        .then(res => res.json())

};
