const MAIN_URL = 'https://api.tvmaze.com/';

export const getMoviesByQuery = (query) => {

    return fetch(MAIN_URL + `search/shows?q=${query}`)

        .then(res => res.json())

};
export const getMovieById = (id) => {

    return fetch(MAIN_URL + `shows/${id}`)

        .then(res => res.json())

};

export const getMovieImage = (id) => {

    return fetch(MAIN_URL + `shows/${id}/images`)

        .then(res => res.json())

};