import './App.css';
import { BrowserRouter as Router, Route, Switch, Redirect } from "react-router-dom";
import Home from "./components/Home";
import SearchPage from './components/SearchPage';
import MovieDetails from './components/MovieDetailsPage';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
          <Redirect path="/" exact to="/home" />
          <Route exact path="/home" component={Home}></Route>
          <Route exact path="/search" component={SearchPage}></Route>
          <Route exact path="/details" component={MovieDetails}></Route>
        </Switch>
      </div>
    </Router>
  );
}

export default App;
