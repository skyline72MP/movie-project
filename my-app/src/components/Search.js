import { useState } from "react";
import { Link } from "react-router-dom";

const Search = () => {

    const [query, setQuery] = useState('');

    return (
        <div className="search-container">
            <input type="search"
                className="form-control"
                id="searchKeywordForm"
                onChange={(e) => {
                    setQuery(e.target.value);
                }}
                placeholder="Search by movie title..." />
            <Link to={`/search?word=${query}`}>Search</Link>
        </div>
    )
};

export default Search;