import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getMovieImage } from "../requests/MovieRequests";


const Favorites = ({ id }) => {

    const [ movieImg, setMovieImg ] = useState(null);
    
    useEffect(() => {
        getMovieImage(id)
            .then(res => setMovieImg(res));
    }, []);

    return (
        <div>
            <Link to={`/details?id=${id}`} >
            { movieImg && <img src={movieImg[0].resolutions.medium.url} alt="img box"></img>}
            </Link>
        </div>
    );
};

export default Favorites;