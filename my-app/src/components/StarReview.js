import { useEffect, useState } from "react";
import { FaStar } from "react-icons/fa";
import { getMovie } from "../requests/MoviesDBRequests";
import { setRatingDB } from "../requests/RatingsDBRequests";

const StarReview = ({ id }) => {
    
    const [ rating, setRating ] = useState(null);
    const [ hover, setHover ] = useState(null);

    const setRatingFunc = (id, value) => {
        setRatingDB(id, value)
            .then(res => setRating(value));
    }
    useEffect(() => {
        getMovie(id)
            .then(res => setRating(res.rating));
    })

    return (
        <div className="stars">
            {[...Array(5)].map((star, i) => {
                const ratingValue = i + 1;

                return (
                <label >
                    <input 
                    type="radio" 
                    name="rating" 
                    value={rating} 
                    onClick={() => setRatingFunc(id, ratingValue)}
                    />
                    <FaStar 
                    className="star" 
                    color={ratingValue <= (hover || rating) ? "#ffc107" : "#e4e5ed"} 
                    size={50}
                    onMouseEnter={() => setHover(ratingValue)}
                    onMouseLeave={() => setHover(null)}
                    />
                </label>
                )
            })}
        </div>
    );
}

export default StarReview;