import { useEffect, useState } from "react";
import { getMovieById } from "../requests/MovieRequests";
import { getMovie } from "../requests/MoviesDBRequests";
import { setNotesDB } from "../requests/NotesDBRequests";
import Navigation from "./Navigation";
import SingleMovieView from "./SingleMovieView";
import StarReview from "./StarReview";

const MovieDetails = () => {

    const id = (new URLSearchParams(window.location.search)).get("id");
    
    const [ movie, setMovie ] = useState(null);
    const [ note, setNote ] = useState(null);

    const changeNote = (value) => {
        setNotesDB(id, value)
            .then(res => setNote(value));
    }
    useEffect(() => {
        if(id) {
        getMovieById(id)
            .then(res => setMovie(res))
    }
    }, [id]);
    useEffect(() => {
        getMovie(id)
            .then(res => setNote(res.note));
    })

    return (
        <div className="movie-details-page">
            <Navigation /> 
            <div className="main-box">
                { movie && <SingleMovieView key={movie.id} id={movie.id} image={movie.image.medium} title={movie.name} genre={movie.genres} desc={movie.summary} />}
                <StarReview id={id} />
                <div className="review">
                    <h2>Your review</h2>
                    <textarea 
                    rows="10" 
                    cols="60" 
                    value={note}
                    onChange={(e) => changeNote(e.target.value)}
                    placeholder="Your private notes and comments about the movie..."></textarea>
                </div>
            </div>
        </div>
    );
};

export default MovieDetails;