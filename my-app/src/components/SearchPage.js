import { useEffect, useState } from "react";
import { getMoviesByQuery } from "../requests/MovieRequests";
import { getMovies, uploadMoviesId } from "../requests/MoviesDBRequests";
import Navigation from "./Navigation";
import Search from "./Search";
import SingleMovieView from "./SingleMovieView";

const SearchPage = () => {

    const word = (new URLSearchParams(window.location.search)).get("word");

    const [movies, setMovies] = useState(null);
    
    useEffect(() => {
        if(word) {
        getMoviesByQuery(word)
            .then(res => setMovies(res));
        };
    }, [word]);
    useEffect(() => {
        movies?.map(m => uploadMoviesId(m.show.id)
        .then(res => console.log(res)))
    }, [movies]);

    return (
        <div className="search-page">
            <Navigation />
            <h1>Search</h1>
            <Search />
            <div className="movies-container">
                {movies?.map(el => <SingleMovieView key={el.show.id} id={el.show.id} image={el.show.image.medium} title={el.show.name} genre={el.show.genres} desc={el.show.summary} />)}
            </div>
        </div>
    );

}

export default SearchPage;