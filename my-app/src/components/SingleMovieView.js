import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getMovie, setFavoriteMovie, unSetFavoriteMovie } from "../requests/MoviesDBRequests";

const SingleMovieView = ({ id, image, title, genre, desc }) => {

    const [ movieData, setMovieData ] = useState(null);
    const [ toggleFavorite, setToggleFavorite ] = useState(false);

    const setFavorite = () => {
        setFavoriteMovie(id)
            .then(res => {console.log(res);
            setToggleFavorite(true)});
    };
    const unSetFavorite = () => {
        unSetFavoriteMovie(id)
            .then(res => {console.log(res);
            setToggleFavorite(false)});
    };
    useEffect(() => {
        getMovie(id)
            .then(res => setMovieData(res))
    }, [id]);

    console.log(movieData?.favorite);
    return (
        <div className="movie-box">
            <img src={image} alt="img box"></img>
            <div className="movie-info">
                <Link to={`/details?id=${id}`}><h2>{title}</h2></Link>
                
                <p>{genre}</p>
                <p>{desc}</p>
                <a href="https://www.tvmaze.com/">Visit Official Site</a> 
                { !toggleFavorite && movieData?.favorite === 0 ? <button onClick={() => setFavorite()} >Add to favorites</button> : <button onClick={() => unSetFavorite()} >Remove from favorites</button>}
                
            </div>
        </div>
    )
};

export default SingleMovieView;