import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { getMovieFavorites } from "../requests/MoviesDBRequests";
import Favorites from "./Favorites";
import Navigation from "./Navigation";

const Home = () => {

    const [ favorites, setFavorites ] = useState([]);
    const map = favorites.map(el => el.movie_id);
    const idArray = map.reduce((acc, el, i) => {
        if(!(map.indexOf(el) !== i)) {
            acc.push(el)
        }
            return acc;
    }, []);
    useEffect(() => {
        getMovieFavorites()
            .then(res => setFavorites(res));
    }, []);
    


    return (
        <div>
            <Navigation />
            <div className="home-style">
                <div className="home-first">
                    <div className="home-left">
                        <h1>Movies</h1>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                            eiusmod tempor incididunt ut labore et dolore magna aliqua
                        </p>
                        <Link to={'/search'}><button>Search</button></Link>
                    </div>
                </div>
                <div className="home-second">
                    <h1>Your Favorites</h1>
                    <div>
                    {idArray?.map(id => <Favorites id={id} />)}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;
