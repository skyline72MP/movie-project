// import { NavLink } from "react-router-dom";

import { NavLink } from "react-router-dom";
import Search from "./Search";

const Navigation = () => {
    return (
        <nav>
            <NavLink activeClassName="active" to="/" exact={true}>
                My Movie Collection
            </NavLink>
            <ul>
                <li>
                    <Search />
                </li>
            </ul>
        </nav>
    );
};

export default Navigation;
