-- MariaDB dump 10.19  Distrib 10.6.2-MariaDB, for Win64 (AMD64)
--
-- Host: 127.0.0.1    Database: movie_project
-- ------------------------------------------------------
-- Server version	10.6.2-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `movies`
--

DROP TABLE IF EXISTS `movies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `movies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `movie_id` int(11) NOT NULL,
  `favorite` tinyint(1) NOT NULL DEFAULT 0,
  `rating` int(11) NOT NULL DEFAULT 0,
  `note` varchar(45) NOT NULL DEFAULT ' ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=431 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movies`
--

LOCK TABLES `movies` WRITE;
/*!40000 ALTER TABLE `movies` DISABLE KEYS */;
INSERT INTO `movies` VALUES (1,34,1,3,'amazings'),(2,8,1,3,'Amazing'),(3,14,0,0,' '),(4,9,1,0,' '),(6,10,0,3,'amazing body'),(7,9,1,0,' '),(8,67,0,0,' '),(9,323,0,0,' '),(10,234,0,0,' '),(11,975,0,1,' hahaha babaab'),(12,19608,0,3,' '),(13,481,0,0,' '),(14,504,0,0,' '),(15,55415,0,0,' '),(16,11464,0,0,' '),(17,3557,0,0,' '),(18,47876,0,0,' '),(19,757,0,0,' '),(20,5951,1,0,' '),(21,975,0,0,' '),(22,19608,0,0,' '),(23,481,0,0,' '),(24,504,0,0,' '),(25,11464,0,0,' '),(26,3557,0,0,' '),(27,55415,0,0,' '),(28,47876,0,0,' '),(29,757,0,0,' '),(30,5951,0,0,' '),(31,975,0,0,' '),(32,19608,0,0,' '),(33,481,0,0,' '),(34,504,0,0,' '),(35,11464,0,0,' '),(36,3557,0,0,' '),(37,55415,0,0,' '),(38,47876,0,0,' '),(39,757,0,0,' '),(40,5951,0,0,' '),(41,975,0,0,' '),(42,19608,0,0,' '),(43,481,0,0,' '),(44,504,0,0,' '),(45,11464,0,0,' '),(46,3557,0,0,' '),(47,55415,0,0,' '),(48,47876,0,0,' '),(49,757,0,0,' '),(50,5951,0,0,' '),(51,975,0,0,' '),(52,55415,0,0,' '),(53,19608,0,0,' '),(54,481,0,0,' '),(401,975,0,0,' '),(402,19608,0,0,' '),(403,481,0,0,' '),(404,504,0,0,' '),(405,11464,0,0,' '),(406,3557,0,0,' '),(407,55415,0,0,' '),(408,47876,0,0,' '),(409,757,0,0,' '),(410,5951,0,0,' '),(411,27436,1,5,' best series ever'),(412,32772,0,0,' '),(413,46749,0,0,' '),(414,30834,0,0,' '),(415,14131,0,0,' '),(416,15700,0,0,' '),(417,16776,0,0,' '),(418,21912,0,0,' '),(419,17986,0,0,' '),(420,145,0,0,' '),(421,27436,0,0,' '),(422,32772,0,0,' '),(423,46749,0,0,' '),(424,30834,0,0,' '),(425,14131,0,0,' '),(426,15700,0,0,' '),(427,16776,0,0,' '),(428,21912,0,0,' '),(429,17986,0,0,' '),(430,145,0,0,' ');
/*!40000 ALTER TABLE `movies` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-24 11:09:04
