import express from 'express';

import notesData from '../data/notes-data.js';
import notesService from '../services/notes-service.js';

export const notesController = express.Router();

notesController
     
    .put('/:id', async (req, res) => {
        const id = req.params.id;
        const { note } = req.body;

        const createdNote = await notesService.setNotes(notesData)(id, note);

        res.status(201).json(createdNote);

    });