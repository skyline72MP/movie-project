import express from 'express';

import moviesData from '../data/movies-data.js';
import moviesService from '../services/movies-service.js';

export const moviesController = express.Router();

moviesController

    .get('/:id', async (req, res) => {
        const id = req.params.id;
        const movie = await moviesService.getMovies(moviesData)(id);
        res.status(200).json(movie);
    })
    .post('/:id', async (req, res) => {
        const id = req.params.id;

        const createdMovie = await moviesService.setMovie(moviesData)(id);

        res.status(201).json(createdMovie);

    })
    .put('/:id', async (req, res) => {
        const id = req.params.id;

        const updatedMovie = await moviesService.setMovieFavorite(moviesData)(id);

        res.status(201).json(updatedMovie);

    })
    .delete('/:id', async (req, res) => {
        const id = req.params.id;

        const updatedMovie = await moviesService.unSetMovieFavorite(moviesData)(id);

        res.status(201).json(updatedMovie);

    })
    .get('/', async (req, res) => {
        const favorite = await moviesService.getFavoriteMovies(moviesData)();

        res.status(200).json(favorite);
    });
    
