import express from 'express';

import ratingsData from '../data/ratings-data.js';
import ratingsService from '../services/ratings-service.js';

export const ratingsController = express.Router();

ratingsController
     
    .put('/:id', async (req, res) => {
        const id = req.params.id;
        const { num } = req.body;

        const createdRating = await ratingsService.setRating(ratingsData)(id, num);

        res.status(201).json(createdRating);

    });
