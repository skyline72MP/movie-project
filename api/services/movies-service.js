
const getMovies = moviesData => {
    return async (id) => await moviesData.getMoviesData(id);
};
const setMovie = moviesData => {
    return async (id) => await moviesData.setMoviesData(id);
};
const setMovieFavorite = moviesData => {
    return async (id) => await moviesData.setFavoriteMovie(id);
};
const unSetMovieFavorite = moviesData => {
    return async (id) => await moviesData.unSetFavoriteMovie(id);
};
const getFavoriteMovies = moviesData => {
    return async () => await moviesData.getFavorites();
};

export default {
    getMovies,
    setMovie,
    setMovieFavorite,
    unSetMovieFavorite,
    getFavoriteMovies
};