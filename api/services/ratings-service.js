
const setRating = ratingsData => {
    return async (id, num) => await ratingsData.setRatingsData(id, num);
};

export default {
    setRating,
};