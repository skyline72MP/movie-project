const setNotes = notesData => {
    return async (id, note) => await notesData.setNotesData(id, note);
};

export default {
    setNotes,
};