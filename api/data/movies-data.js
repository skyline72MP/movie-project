
import pool from './pool.js';

const setMoviesData = async (id) => {
    const sql = `
    INSERT INTO
    movies (movie_id) values(?) `;

    const result = await pool.query(sql, id);

    return result;
};

const getMoviesData = async (id) => {
    
    const result = await pool.query(`select id, favorite, rating, note from movies where movie_id = ?`, id);
    
    return result[0];
};

const setFavoriteMovie = async (id) => {

    const sql = `
    select id from movies
    where movie_id = ? `;
    
    const resultId = await pool.query(sql, id);

    const sql2 = `
    update movies 
    set favorite = 1
    where id = ? `;

    const result = await pool.query(sql2, resultId[0].id);

    return result;

};

const unSetFavoriteMovie = async (id) => {

    const sql = `
    select id from movies
    where movie_id = ? `;
    
    const resultId = await pool.query(sql, id);

    const sql2 = `
    update movies 
    set favorite = 0
    where id = ? `;

    const result = await pool.query(sql2, resultId[0].id);

    return result;

};

const getFavorites = async () => {

    const sql = `select movie_id from movies
    where favorite = 1`;

    const result = await pool.query(sql);

    return result;
};

export default {
    getMoviesData,
    setMoviesData,
    setFavoriteMovie,
    unSetFavoriteMovie,
    getFavorites   
};
