import pool from './pool.js';

const setRatingsData = async (id, num) => {

    const sql = `
    select id from movies
    where movie_id = ? `;
    
    const resultId = await pool.query(sql, id);

    const sql2 = `
    update movies 
    set rating = ?
    where id = ? `;

    const result = await pool.query(sql2, [num, resultId[0].id]);

    return result;

};

export default {
    setRatingsData,
};