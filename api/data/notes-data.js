import pool from './pool.js';

const setNotesData = async (id, note) => {

    const sql = `
    select id from movies
    where movie_id = ? `;
    
    const resultId = await pool.query(sql, id);

    const sql2 = `
    update movies 
    set note = ?
    where id = ? `;

    const result = await pool.query(sql2, [note, resultId[0].id]);

    return result;

};

export default {
    setNotesData,
};