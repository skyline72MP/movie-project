import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import dotenv from 'dotenv';
import passport from 'passport';

import { moviesController } from './controllers/movies-controller.js';
import { ratingsController } from './controllers/ratings-controller.js';
import { notesController } from './controllers/notes-controller.js';

const app = express();

const config = dotenv.config().parsed;
const PORT = +config.PORT;

app.use(express.json());
app.use(cors());
app.use(helmet());
app.use(passport.initialize());

app.use('/movies', moviesController);
app.use('/ratings', ratingsController);
app.use('/notes', notesController);

// app.all('*', (req, res) =>
//     res.status(400).json({ message: 'Resource not found!' })
// );

app.listen(PORT, () => console.log(`App is listening ar port ${PORT}`));